
Python social network bot.
========

Пока что просто чат бот. Со временем может быть будут добавлены полезные функции, добавлени ИИ и другие плюшки.
Сделан на основе https://github.com/4ndv/lolbot и https://github.com/palyzx/lolbot

Название изменилось, и сильно изменитья структура, так что не знаю на сколько это форк. 

**ВНИМАНИЕ:**

Для работы бота необходим Python версии 2.7.x, с версией 3 бот **не работает**

Изначальной идеей было соеденить ИИ с аккаунтом вконтакте, для фана и обучения ИИ. Но ИИ пока не найден, или не написан.

Работоспособность проверена исключительно на ОС семейства Linux. Частично проверена работа в Windows (Windows XP).

## Первый запуск

1. Установить модуль vk_api: `pip install vk_api`
                     yaml: `pip install pyyaml`
		     zmq: `pip install pyzmq`
2. Переименовать `settings.yml.sample` в `settings.yml`. В этом файле заменить на `login` и `password` на логин и пароль соответственно. 
3. При необходимости в `settings.yml` можно указать (`vk_app_id =`) ID другого приложения вк вместо стандартного app_id из модуля vk_api. 
Значение `-1` указывает на использование app_id по умолчанию.
4. Прописать другие настройки
5. Запустить: `./loader.sh` 

Для теста можно запускать `python system.py`, но не будет доступен перезапуск из плагина, или перезапуск при необрабатываемой ошибке.

## Настройки `settings.yml`

`path` путь поиска плагиной. строковый.
`prefixes` префиксы команды. список строк.
`startsymbols` стартовый символ команды. список строк. (введён для упрощения подачи команд. пишете боту символ и команду без пробела. аргументы уже через пробел задавать)
`cmd_in_own_messages` обрабатывать и исходящие сообщения тоже. логический. (Введён для возможности запускать бота как помошника на основном аккаунте)
`be_online` поддерживать статус "онлайн". логический.

## Плагины

Их стало много. Запускайте плагин `info_sys.py` на работающем боте или смотрите файлы в `plugins/`.
Введены разные типы плагинов.

Для плагинов, которые загружают данные из сети нужно наличие папки `webm/`. Пока что не вынесено в настройки. 

* Расписать подробнее *

## Создание плагинов

В папках `plugins_core` и `handlers_core` лежат шаблоны для наследования. Примеры написания в `plugins/`.

* Расписать подробнее *

Плагины размещаются в папке, определённой в `path` в `settings.yml`. 

Плагины могут работать со всеми методами API вконтакте, кроме фоновых процессов плагинов. В последних нужно или созадвать ещё один vk_api(но это не правильно, и так легко можно превысить ограничение по запросам к vk api) или переделывать полностью систему. Что будет в будущем. 

## Лицензия

GPL v2.


