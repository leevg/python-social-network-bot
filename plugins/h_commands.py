# -*- coding: utf-8 -*-


from handlers_core.command_exec_settings import CommandExec


import os
import sys
import random

__version__ = '0.1.0'

class MessageHandler(CommandExec):
    
    name = u'Generic commands handler'

    version = __version__

    def parce(self, message):
        if message['body'] == u'':
            return {}

        words = message['body'].split()     
        plugin = ''
        
        if len(words) > 1 and words[0].lower() in self.settings['prefixes']:
            plugin = words[1].lower()
            params = words[2:]
        elif words[0][0] in self.settings['startsymbols']:
            plugin = words[0][1:].lower()
            params = words[1:]    

        if plugin == '':
            return {}
        else:
            print('> ' + message['body'])
            return {'plugin':plugin, 'params':params}
            
            




