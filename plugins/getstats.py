# -*- coding: utf-8 -*-

import random
from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'

class Plugin(CommandPlugin):

    keys = [u'статусы', u'getstats']
    name = u'Пример плагина команды'

    version = __version__

    def call(self, msg):

        answ_str_stats = [u'Счётчики', u'Количества', u'Счётчики аккаунта',
            u'Account stats', u'Account counters']
        answ_str_stats_null = [u'Всё по нулям', u'Всё счётчики по нулям',
            u'All are null', u'Account stats are null', u'Account counters = 0']

        stats = self.vk.method('account.getCounters')

        stats_str = ''

        for key in stats:
            stats_str += u'* ' + key + u' = ' + str(stats[key]) + u'\n'

        if stats_str == '':
            answ = random.choice(answ_str_stats_null) + u'.'
        else:
            answ = random.choice(answ_str_stats) + u' : \n' + stats_str

        self.vk.respond(msg, {'message': answ})
