# -*- coding: utf-8 -*-

import os
import platform
import random

from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'

class Plugin(CommandPlugin):

    keys = [u'сисинфо', 'sysinfo', u'инфосис', 'infosys']
    name = u'Информация о системе бота с типом плагина "command system"'
    info = u'Выдаёт информацию о доступных плагинах, их описания и список команд. Так же выдаёт название и версию ОС, на которой запущена система бота.'

    version = __version__

    plugin_type = CommandPlugin.plugin_type + ' system args'

    short_answ = True

    def call(self, msg, args, system):
        answers = []
        answers.append(u"Внимание! Работает бот. Вот инфа про него.")
        answers.append(u"Информация про бота:")
        answers.append(u"Я бот. Мой создатель потихоньку мозги мне прикручивает. Вот инфо.")

        answers_saved = []
        answers_saved.append(u"Сохранено.")
        answers_saved.append(u"Настройка сохранена.")
        answers_saved.append(u"Saved.")
        answers_saved.append(u"Сохранилсо.")
        answers_saved.append(u"Выполнено.")
        answers_saved.append(u"Вас понял. Выполнено.")

        answers_mode = []
        answers_mode.append(u"Режим ответа:")
        answers_mode.append(u"Answer mode is:")
        answers_mode.append(u"Акуна матата:")

        cmd_short = [u'кратко', 'short']
        cmd_verbose = [u'подробно', 'verbose']
        cmd_save = [u'сохранить', 'save']
        cmd_mode = [u'режим', 'mode']

        strs_ver = ['ver', u'вер']

        short_answ = self.short_answ

        for arg in args:
            if arg.lower() in cmd_short:
                short_answ = True
            elif arg.lower() in cmd_verbose:
                short_answ = False

        for arg in args:
            if arg.lower() in cmd_save:
                self.short_answ = short_answ
                self.vk.respond(msg, {'message': random.choice(answers_saved)})
                return

        for arg in args:
            if arg.lower() in cmd_mode:
                if self.short_answ:
                    answ_mode = random.choice(answers_mode) + " " + \
                        random.choice(cmd_short) + "."
                else:
                    answ_mode = random.choice(answers_mode) + " " + \
                        random.choice(cmd_verbose) +"."
                self.vk.respond(msg, {'message': answ_mode})
                return


        lists = ''

        for plugin in system.plugins:
            lists += '*** ' + plugin
            if hasattr(system.plugins[plugin], 'version'):
                lists += ' (' + random.choice(strs_ver) + ':' + \
                    system.plugins[plugin].version + ')'

            lists += u'\n'


            lists += u'Комманды: '
            cmdstr = u''
            for cmd in system.commands:
                if system.commands[cmd] == system.plugins[plugin]:
                    cmdstr += cmd + u', '
            lists += cmdstr[:-2] + u'\n'
            lists += u'\n'

            if hasattr(system.plugins[plugin], 'name') and not short_answ:
                lists += u'Название: ' + system.plugins[plugin].name + u'\n'
            if hasattr(system.plugins[plugin], 'info') and not short_answ:
                lists += u'Описание: ' + system.plugins[plugin].info + u'\n'
            lists += u'\n'

        answ_str = random.choice(answers) + u'\n' + \
            u'python-social-network-bot \n system ver.:' + system.version
        answ_str += u' \n \n Загруженные плагины:\n' + lists + \
            u'\n \n ОС ' + platform.platform()
        if platform.node() != '':
            answ_str += u'\n Hostname: ' + platform.node()

        self.vk.respond(msg, {'message': answ_str})
