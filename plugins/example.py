# -*- coding: utf-8 -*-

__version__ = '0.1.0'

class Plugin:
    vk = None
    #plugin_type = 'command'
    # 'command' - команда
    # 'command background' - команда с фоновым процессом
    # 'messages analyser' - анализатор сообщений
    # 'messages analyser background' - анализатор сообщений с фоновым процессом

    keys = [u'примерплагина', u'тестовыйплагин']
    name = u'Пример плагина'

    version = __version__

    def __init__(self, vk):
        self.vk = vk
        print(self.name)

    def call(self, msg):
        self.vk.respond(msg, {'message': self.name})