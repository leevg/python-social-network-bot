# -*- coding: utf-8 -*-

from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'

class Plugin(CommandPlugin):
    # 'command' - команда
    # 'command background' - команда с фоновым процессом
    # 'messages analyser' - анализатор сообщений
    # 'messages analyser background' - анализатор сообщений с фоновым процессом

    keys = [u'примерплагинакоманды', u'plugin_command_example']
    name = u'Пример плагина команды'

    version = __version__

    def call(self, msg):
        self.vk.respond(msg, {'message': self.name})