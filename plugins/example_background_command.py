# -*- coding: utf-8 -*-

import time
from plugins_core.command_plugin import CommandPlugin
from multiprocessing import Process
from vkservice import VkServiceClient


__version__ = '0.1.0'

class Plugin(CommandPlugin):
    # 'command' - команда
    # 'command background' - команда с фоновым процессом
    # 'messages analyser' - анализатор сообщений
    # 'messages analyser background' - анализатор сообщений с фоновым процессом

    keys = [u'plugin_background_command_example']
    name = u'Пример плагина команды с фоновым процессом'

    version = __version__
    
    def __proc(self, msg):
        print "ololo"
        time.sleep(20)
        print "ololo2"
        vkclient = VkServiceClient()
        vkclient.respond(msg, {'message': self.name})

    def call(self, msg):
        proc = Process(target=self.__proc, args=(msg,))
        proc.start()
        self.vk.respond(msg, {'message': u'Запущено'})

