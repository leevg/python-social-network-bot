# -*- coding: utf-8 -*-

from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'

class Plugin(CommandPlugin):
    # 'command' - команда
    # 'command background' - команда с фоновым процессом
    # 'messages analyser' - анализатор сообщений
    # 'messages analyser background' - анализатор сообщений с фоновым процессом

    keys = [u'send', u'написать', u'лс', u'msg']
    name = u'Send message'

    version = __version__

    plugin_type = CommandPlugin.plugin_type + ' args'

    def call(self, msg, args):

    	if len(args) >= 2:
    		uid=int(args[0])
    		body=''
    		for arg in args[1:]:
    			body = body + ' ' + arg
    		val = {
    		#'user_id':uid,
    		'peer_id':uid,
    		'message':body
    		}
    		self.vk.method('messages.send',val)
    	else:
			self.vk.respond(msg, {'message': 'Кому? Что?'})