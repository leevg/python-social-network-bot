# -*- coding: utf-8 -*-

# standard library imports

#from ast import literal_eval


from multiprocessing import Process
import os
import random

#import shlex
import sys

# related third party imports

import yaml

# local application/library specific imports
from plugins_core.command_plugin import CommandPlugin
from vkservice import VkServiceClient
from vkvideo import VkVideoGet

__version__ = '0.1.3'


class Plugin(CommandPlugin):
    vk = None

    name = u'VK video saver'
    keys = ['vkvideosave', 'savevkv']

    plugin_type = CommandPlugin.plugin_type + ' args settings'

    version = __version__

    def __login(self,vkvget):

        with open('settings.yml', 'r') as file:
             settings = yaml.load(file)

        email = settings['vk_login']
        pwd = settings['vk_password']
    
        vkvget.login(email, pwd)

    def __proc(self, msg, args, settings):

        vkclient = VkServiceClient(settings['zmq_port'])

        answers = []
        answers.append(u"Maybe got it.")
        answers.append(u"Наверное загрузилось.")
        answers.append(u"я хз, я wget'у передал, но обрабатывать его ответ ещё не умею.")

        answers_nothing = [u"Ничего и не загружалось. Не те типы файлов по ссылкам.",
            u"Не те типы файлов по ссылкам. Ничего и не загружалось.",
            u"Ничего не загружено, не те ссылки.",
            u"Ссылки не подходящие, ничего не загружал"]
        answers_all_error = [u"Ничего не загрузилось.",
            u"Не получилось скачать.", u"Всё - ошибки."]
        answers_count = [u"Количество", u"Вот столько", u"Count"]
        answers_count_ok = [u"Загружено успешно", u"Количество успешних",
            u"Count ok"]
        answers_count_error = [u"Не загружено", u"Количество ошибок",
            u"Count of errors"]
        answers_all_ok = [u"Всё загрузилось.", u"Всё ок.",
            u"Получилось загрузить всё."]
        answers_50_50 = [u"Частично загрузилось.", u"Загрузились, но не все.",
            u"Некоторые получились."]

        get_dir = './webm/'

        count_ok = 0
        count_error = 0

	vkvget = VkVideoGet()

        self.__login(vkvget) # это нужно как-то узаконить, и перестать постоянно логиниться.
                # например - сохранять куки.

        for word in args:

            if (('http://' in word[0:7]) or ('https://' in word[0:9])) and ('vk.com/video' in word):
                result = vkvget.get_url(word)

                #command = 'wget -a dl.log ' + '-O "'  + get_dir + result['fname'] + '" "' + result['url'] + '"'
                command = 'aria2c -x 5 -s 5 ' + '-o "'  + get_dir + \
                    result['fname'] + '" "' + result['url'] + '"'
                print command
                exitcode = os.system(command.encode('utf-8'))
                if exitcode == 0:
                    count_ok += 1
                else:
                    count_error += 1

        answ_str = random.choice(answers) + u'\n'

        if (count_ok == 0) and (count_error == 0):
            answ_str += random.choice(answers_nothing)
        elif count_ok == 0:
            answ_str += random.choice(answers_all_error)
            answ_str += u'\n'
            answ_str += random.choice(answers_count) + u':' + str(count_error)
        elif count_error == 0:

            answ_str += random.choice(answers_all_ok)
            answ_str += u'\n'
            answ_str += random.choice(answers_count) + u':' + str(count_ok)
        else:
            answ_str += random.choice(answers_50_50)
            answ_str += u'\n'
            answ_str += random.choice(answers_count_ok) + u':' + str(count_ok)
            answ_str += u'\n'
            answ_str += random.choice(answers_count_error) + u':' + \
                        str(count_error)

        vkclient.respond(msg, {'message': answ_str})

    def call(self, msg, args, settings):

        answers_none = []
        answers_none.append(u"Nothing to get")
        answers_none.append(u"Ничего нет")
        answers_none.append(u"А ссылка где?")
        answers_none.append(u"Пустой зарос =(")

        answers_processing = [u"Запущено", u"Запрос обрабатывается..",
            u"чотто матте кудасай", u"Please wait"]

        if len(args) == 0:
            self.vk.respond(msg, {'message': random.choice(answers_none)})
            return

        proc = Process(target=self.__proc, args=(msg, args, settings, ))
        proc.start()

        self.vk.respond(msg, {'message': random.choice(answers_processing)})
