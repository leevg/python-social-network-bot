# -*- coding: utf-8 -*-

import os
#import sys
import platform
import random
from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'

class Plugin(CommandPlugin):
    
    keys = [u'выход', u'exit', u'quit', u'выйти']
    name = u'Выход'
    info = u'Завершает бота.'
    plugin_type = CommandPlugin.plugin_type + ' system args'

    version = __version__

    short_answ = True

    def call(self, msg, args, system):
        answers = []
        answers.append(u"Внимание! Выключаюсь.")
        answers.append(u"Выключаюсь...")
        answers.append(u"Exiting...")
        
        answ_str = random.choice(answers) 

        system.core_restart = False
        self.vk.respond(msg, {'message': answ_str})
        system.core_exit_run = True