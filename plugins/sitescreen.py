# -*- coding: utf-8 -*-

import vk_api
import random

from selenium import webdriver
import time
import StringIO
from PIL import Image

import yaml
from vkplus import VkPlus

from multiprocessing import Process
from plugins_core.background_plugin_with_args import BackgroundPluginWithArgs

__version__ = '0.1.0'

class Plugin(BackgroundPluginWithArgs):

    keys = [u'скрин_сайта', u'site_screen']
    name = u'Site srceen'

    version = __version__

    def __proc(self, msg, bg_mess_queue, arg):

        with open('settings.yml', 'r') as file:
            settings = yaml.load(file)

        answers = []
        answers.append(u"Ваш скрин:")
        answers.append(u"Скрин готов:")
        answers.append(u"Ура, получилось!")

        session_vk = VkPlus(settings['vk_login'], settings['vk_password'],
                            settings['vk_app_id'])

        # Нужно логинить отдельно
        # Или реализовать это в vkplus
        upload = vk_api.VkUpload(session_vk.api)

        print(u'Start')
        driver = webdriver.PhantomJS(service_args=['--ssl-protocol=any']) # or
                                                # add to your PATH
        driver.set_window_size(1024, 768) # optional
        driver.get(arg)
        time.sleep(5)
        print(u'scr')
        #driver.save_screenshot('screen_tmp.png')
        screen = driver.get_screenshot_as_png()
        box = (0, 0, 1024, 768)
        im = Image.open(StringIO.StringIO(screen))
        region = im.crop(box)
        region.save('screen_tmp.jpg', 'JPEG', optimize=True, quality=95)
        print(u'quit silenium')
        driver.quit()

        photo = upload.photo_messages('screen_tmp.jpg')
        owner_id = str(photo[0]['owner_id'])
        att_id = str(photo[0]['id'])
        attachment = 'photo' + owner_id + '_' + att_id

        answ = {
            'in_msg' : msg,
            'answ' : {'message' : random.choice(answers),
                      'attachment' : attachment}
        }
        bg_mess_queue.put(answ)

    def call(self, msg, args, bg_mess_queue):

        answers = []
        answers.append(u"Запрос принят.")
        answers.append(u"Запрос обрабатывается.")
        answers.append(u"Принято. Работаю.")

        answers_error = []
        answers_error.append(u"А что?")
        answers_error.append(u"А где ссылка?")
        answers_error.append(u"А что скринить?")


        if len(args) > 0:
            proc = Process(target=self.__proc, args=(msg, bg_mess_queue,
                                                    args[0], ))
            proc.start()

            self.vk.respond(msg, {'message': random.choice(answers)})
        elif random.randint(0, 3) == 1:
            self.vk.respond(msg, {'message': random.choice(answers_error)})
