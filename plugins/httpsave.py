# -*- coding: utf-8 -*-

from plugins_core.command_plugin import CommandPlugin
from handlers_core.command_exec import CommandExec
from multiprocessing import Process
from vkservice import VkServiceClient

import os
import sys
import random

__version__ = '0.1.1'



def string_test_light(word):
    return (('http://' in word[0:7]) or ('https://' in word[0:9]))

def string_test(word):
    file_ext_3 = [u'.jpg', u'.png', u'.gif']
    file_ext_4 = [u'.jpeg', u'.webm']

    return (string_test_light(word) and ((word[-4:] in file_ext_3) or (word[-5:] in file_ext_4)))

class MessageHandler(CommandExec):

    name = u'http https links handler'

    version = '0.1.0'

    def parce(self, message):
        # А если сообщение только с аттачем ?
        if message['body'] == u'':
            # Нечего обрабатывать
            return {}

        words = message['body'].split()
        params =''

        # Если в начале сообщения не ссылка - такое игнорить.
        # Потом добавить настройку на каждого пользователя Х)
        if not string_test(words[0]):
            return {}

        for word in words:
            if string_test(word):
                params += word + u' '

        params = params.split()

        if len(params) != 0:
            print('> ' + message['body'])
            return {'plugin':'savehttp', 'params':params}
        else:
            return {}



class Plugin(CommandPlugin):
    vk = None

    version = __version__

    keys = ['savehttp', 'saveweb']
    name = u'http https saver'

    plugin_type = CommandPlugin.plugin_type + ' args settings'

    def __proc(self, msg, args, settings):

        vkclient = VkServiceClient(settings['zmq_port'])

        answers = []
        answers.append(u"Maybe got it.")
        answers.append(u"Наверное загрузилось.")
        answers.append(u"я хз, я wget'у передал, но обрабатывать его ответ ещё не умею.")

        answers_nothing = [u"Ничего и не загружалось. Не те типы файлов по ссылкам.", u"Не те типы файлов по ссылкам. Ничего и не загружалось.", u"Ничего не загружено, не те ссылки.", u"Ссылки не подходящие, ничего не загружал"]
        answers_all_error = [u"Ничего не загрузилось.",
            u"Не получилось скачать.", u"Всё - ошибки."]
        answers_count = [u"Количество", u"Вот столько", u"Count"]
        answers_count_ok = [u"Загружено успешно", u"Количество успешних",
            u"Count ok"]
        answers_count_error = [u"Не загружено", u"Количество ошибок",
            u"Count of errors"]
        answers_all_ok = [u"Всё загрузилось.", u"Всё ок.",
            u"Получилось загрузить всё."]
        answers_50_50 = [u"Частично загрузилось.", u"Загрузились, но не все.",
            u"Некоторые получились."]

        get_dir = './webm/'

        count_ok = 0
        count_error = 0

        for word in args:
            if string_test_light(word):
                command = 'wget -a dl.log -P "' + get_dir + '" "' + word + '"'
                print command
                exitcode = os.system(command)
                if exitcode == 0:
                    count_ok += 1
                else:
                    count_error += 1

        answ_str = random.choice(answers) + u'\n'

        if (count_ok == 0) and (count_error == 0):
            answ_str += random.choice(answers_nothing)
        elif count_ok == 0:
            answ_str += random.choice(answers_all_error)
            answ_str += u'\n'
            answ_str += random.choice(answers_count) + u':' + str(count_error)
        elif count_error == 0:

            answ_str += random.choice(answers_all_ok)
            answ_str += u'\n'
            answ_str += random.choice(answers_count) + u':' + str(count_ok)
        else:
            answ_str += random.choice(answers_50_50)
            answ_str += u'\n'
            answ_str += random.choice(answers_count_ok) + u':' + str(count_ok)
            answ_str += u'\n'
            answ_str += random.choice(answers_count_error) + u':' + \
                str(count_error)

        vkclient.respond(msg, {'message': answ_str})

    def call(self, msg, args, settings):

        answers_none = []
        answers_none.append(u"Nothing to get")
        answers_none.append(u"Ничего нет")
        answers_none.append(u"А ссылка где?")
        answers_none.append(u"Пустой зарос =(")

        answers_processing = [u"Запущено", u"Запрос обрабатывается..",
            u"чотто матте кудасай", u"Please wait"]

        if len(args) == 0:
            self.vk.respond(msg, {'message': random.choice(answers_none)})
            return

        proc = Process(target=self.__proc, args=(msg, args,settings,))
        proc.start()

        self.vk.respond(msg, {'message': random.choice(answers_processing)})
