# -*- coding: utf-8 -*-

import os
import platform
import random
from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'

class Plugin(CommandPlugin):
    
    keys = [u'restart', u'рестарт', u'перезагрузись', u'перезагрузка']
    name = u'Restart'
    info = u'Перезапускает бота.'
    plugin_type = CommandPlugin.plugin_type + ' system args'
    
    version = __version__

    short_answ = True

    def call(self, msg, args, system):
        answers = []
        answers.append(u"Внимание! Перезагрузка.")
        answers.append(u"Перезапускаюсь...")
        answers.append(u"Restarting...")
        
        answ_str = random.choice(answers) + u'\n Запуск через минимум минуту.'

        system.core_restart = True
        self.vk.respond(msg, {'message': answ_str})
        system.core_exit_run = True