# -*- coding: utf-8 -*-

import random
from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'

#ptype = 'command with_args' # Потом сделать с аргументами
class Plugin(CommandPlugin):
    keys = [u'тортик', u'нямка', u'печенько' ]
    name = u'Тортики.'
    info = u'Выдаёт рандомно картинку со стены группы https://vk.com/cakes_of_umari . Аргументы: одно число - будет искать столько картинок и слать по одной, два числа - количество картинок и максимум в сообщении (от 2 до 9), число pack/пак. Если вывод паком, указано число в паке и в конце написать "не" - то будет ровными паками выдавать, без рандома.'
    plugin_type = CommandPlugin.plugin_type + ' args'

    version = __version__

    def call(self, msg, args):
        answers = []
        answers.append(u"Смачного!")
        answers.append(u"Дивися не подавися!")
        answers.append(u"Ось ваше замовлення:")

        pack_str=[u"пак",u"куча",u"pack"]
        no_rand_pack_str=[u"norand",u"нерандом",u"не"]

        wall = None
        out_packs = False
        no_rand_pack = False
        max_pack = 9

        grp_ids = []
        grp_ids.append(-103628621)

        if len(args) > 0 and args[0].isdigit():
            trycount = int(args[0])
        else:
            trycount = 1

        if len(args) >1:
            if args[1].isdigit() and (int(args[1])>1) and \
                    (int(args[1])<10):
                max_pack = int(args[1])
                out_packs = True
            elif (args[1] in pack_str):
                out_packs = True

        if len(args) >2:
            if args[2].isdigit() and (int(args[2])>1) and \
                    ( int(args[2])<10):
                max_pack = int(args[2])
                out_packs = True
            elif args[2] in no_rand_pack_str:
                no_rand_pack = True

        if len(args) >3 and (args[3] in no_rand_pack_str):
            no_rand_pack = True


        attachments = []

        print(u'2d working')

        old_grp_id=0

        for i in range(0,trycount):

            grp_id = random.choice(grp_ids)

            if grp_id != old_grp_id:
                values = {
                         'owner_id': grp_id,
                         'offset': 0,
                         'count': 1
                }

                wall = self.vk.method('wall.get', values)

                items_count = wall['count']

            isphoto = False

            while isphoto is False:
                values = {
                     'owner_id': grp_id,
                     'offset': random.randint(0, items_count),
                     'count': 1
                }
                wall = self.vk.method('wall.get', values)
                photos = []

                if len(wall['items'])>0:
                    if 'attachments' in wall['items'][0]:
                        for attachment in wall['items'][0]['attachments']:
                            if 'photo' in attachment:
                                photos.append(attachment['photo'])
                                isphoto = True

            wall_att = random.choice(photos)


            owner_id = str(wall_att['owner_id'])
            att_id = str(wall_att['id'])
            if 'access_key' in wall_att:
                access_key = str(wall_att['access_key'])
                attachment = 'photo' + owner_id + '_' + att_id + '_' + \
                    access_key
            else:
                attachment = 'photo' + owner_id + '_' + att_id
            attachments.append(attachment)

            old_grp_id = grp_id

        if out_packs:

            while len(attachments) > 0:
                attachments_str = ''

                if no_rand_pack and (len(attachments)>max_pack):
                    rnd = max_pack
                else:
                    if len(attachments) > max_pack:
                        rnd = random.randint(1,max_pack)
                    else:
                        rnd = random.randint(1,len(attachments))

                print('len: '+str(len(attachments))+' rnd: '+ str(rnd))

                for i in range(0, rnd):
                    attachments_str = attachments_str + attachments.pop() + ','

                attachments_str = attachments_str[:-1]


                self.vk.respond(msg, {'message': random.choice(answers) + \
                    u'\n Count of posts on the wall: ' + \
                    str(items_count), 'attachment': attachments_str})
        else:
            for attachments_str in attachments:
                if random.randint(1,3)==2:
                    self.vk.respond(msg, {'message': random.choice(answers) + \
                        u'\n Count of posts on the wall: ' + \
                        str(items_count), 'attachment': attachments_str})
                else:
                    self.vk.respond(msg, {'attachment': attachments_str})



        print(u'2d done')
