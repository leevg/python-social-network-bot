# -*- coding: utf-8 -*-

import random
from plugins_core.command_plugin import CommandPlugin

__version__ = '0.1.0'


class Plugin(CommandPlugin):
    keys = [u'рандом', u'ранд', 'random', 'rand', 'dice', u'кубик']
    name = u'Рандом.'
    info = u'Просто рандом. У него может быть 1 или 2 целочисленных параметра. \n Если задан один параметр: то будет генерировать целые числа между нулём и заданным числом. Если два - то меджу заданными числами.'
    plugin_type = CommandPlugin.plugin_type + ' args'
    
    version = __version__

    def call(self, msg, args):
        #args = msg['body'].split()
        nums = []
        if len(args) > 0 and args[0].lstrip('-').isdigit():
            nums.append(int(args[0]))
            if len(args) > 1 and args[1].lstrip('-').isdigit():
                nums.append(int(args[1]))
            else:
                nums.append(0)
        else:
            nums = [1, 6]

        nums.sort()
        num = random.randint(nums[0], nums[1])

        self.vk.respond(msg, {'message': str(num)})

        return

# Помогал Антон, нужно его добавить в копирайты