# -*- coding: utf-8 -*-


# standard library imports

from multiprocessing import Process
import signal
import sys
import time

# related third party imports
import yaml

# local application/library specific imports
from botcore import BotCore
from vkservice import VkServiceServer


# подвязать к git
__version__ = '0.2.2'


# -=- Start here -=-

if __name__ == '__main__':
    with open('settings.yml', 'r') as file:
        settings = yaml.load(file)

    core = BotCore(settings,__version__)

    vkapi_service = VkServiceServer(core.settings['vk_login'],
                                    core.settings['vk_password'],
                                    core.settings['vk_app_id'],
                                    core.settings['zmq_port'])

    #Ctrl-C
    signal.signal(signal.SIGINT, core.terminate)
    #kill
    signal.signal(signal.SIGTERM, core.terminate)

    print(u'Запуск сервиса доступа к vkapi.')

    vkapi_service_proc = Process(target=vkapi_service.run, args=())
    vkapi_service_proc.start()

    core.run()

    print(u'wait proc exit')
    vkapi_service_proc.join()

    if core.core_restart:
        print(u'Перезапуск...')
        # Затычка что бы цыклически не перезагружалось
        time.sleep(51)
        print(u'Перезапуск!')
        sys.exit(1)
    else:
        print(u'Выход')
        sys.exit(0)
