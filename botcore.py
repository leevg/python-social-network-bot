# -*- coding: utf-8 -*-

# standard library imports
import imp
import glob
import os
import time
import sys

# local application/library specific imports
#from vkservice import VkServiceServer, VkServiceClient
from vkservice import VkServiceClient

class BotCore:
    def __init__(self, settings, common_ver=None):
        path = settings['path']
        self.version = common_ver

        # -Вынести в модуль "commands"-
        # Нет, пока что здесь остаються.
        self.commands = {}
        self.plugins = {}
        self.messagehandlers = {}
        self.lastmessid = 0

        self.core_exit_run = False
        self.core_restart = False
        self.core_kill_signal = False

        # Значения по умолчанию. Вынести в отдельный файл.
        # И вообще это вынести от сюдава. Или нет?
        self.be_online = False
        self.set_offline = False
        self.cmd_in_own_messages = False

        # Надо это как-то упорядочить, а то разрастёться и засрёться.
        if settings.has_key('cmd_in_own_messages'):
            self.cmd_in_own_messages = settings['cmd_in_own_messages']

        if settings.has_key('be_online'):
            self.be_online = settings['be_online']

        if settings.has_key('set_offline'):
            self.set_offline = settings['set_offline']

        # Это отдельный случай
        if not settings.has_key('zmq_port'):
            settings['zmq_port'] = 43000

        # Переписываем настройки.
        self.settings = settings

        print(u'Python social network bot.')
        print(u'VK.COM autorization...')
        print(u'Login: ' + settings['vk_login'] )
        print(u'ZMQ port: '+ str(settings['zmq_port']))

        # VkPlus нужно переделывать. В нём делать цикл. Цикл читает или
        # из queue или подобного, и отвечает тоже туда же.
        # Логины и пароли вынести в отдельный файл. Что бы всем
        #подряд модулям не передавать.

        #P001
        #self.vkapi_service = VkServiceServer(settings['vk_login'],
        #                                    settings['vk_password'],
        #                                    settings['vk_app_id'],
        #                                    settings['zmq_port'])

        self.vkclient = VkServiceClient(settings['zmq_port'])

        print(u'Loading plugins...')
        print(u'---------------------------')

        # Подгружаем модули
        # Это вынести как класс - загрузчик модулей(плагинов).
        # И тут же грузить модули. У модуля будут свои плагины.

        for module_path in glob.glob(path + '*.py'):
            nothing_reg = True
            module_name = os.path.basename(module_path).split('.')[0]
            try:
                module = imp.load_source(module_name, module_path)
            except:
                print(u"Bad module: " + module_path)
                # нужно как-то печатать тип ошибки Х)

            else:
                # Проверяем наявность обработчика команды в модуле.
                if module and hasattr(module, 'Plugin') and hasattr(module.Plugin, 'plugin_type'):
                    # Создаём объект класса
                    self.plugins[module_name] = module.Plugin(self.vkclient)
                    if hasattr(module.Plugin, 'version'):
                        print('Command: ' + module.Plugin.name + ' ver: ' + module.Plugin.version)
                    else:
                        print('Command: ' + module.Plugin.name)
                    nothing_reg = False

                # Проверяем наявность обработчика сообщений в модуле.
                if module and hasattr(module, 'MessageHandler') and hasattr(module.MessageHandler, 'messagehandler_type'):
                    # Создаём объект класса
                    args = []
                    if 'settings' in module.MessageHandler.messagehandler_type:
                        args.append(settings)
                    self.messagehandlers[module_name] = module.MessageHandler(*args)
                    if hasattr(module.MessageHandler, 'version'):
                        print('Handler: ' + module.MessageHandler.name + ' ver: ' + module.MessageHandler.version)
                    else:
                        print('Handler: ' + module.MessageHandler.name)
                    nothing_reg = False

                # Ничего не нашли - предупредим об этом.
                if nothing_reg:
                    print(u"Wrong, old type or empty module: " + module_path)

        print(u'---------------------------')

        # Создаём словарь "команда" - обработчик команды
        for plugin in self.plugins.values():
            if 'command' in plugin.plugin_type:
                for key in plugin.keys:
                    self.commands[key] = plugin


    # Это можно вынести отдельным модулем. Или вообще преобразовать
    # обработку сообщений.
    # Обработка непрочитанных сообщений. Ищет диалоги с непрочитанными
    # сообщениями. Находит сообщения и передаёт их стандартному обработчику.
    def parce_unread(self):
        def _getDialogs_unread(offset,count):
            values = {
            'unread': 1,
            'offset': 0,
            'count': 200,
            'preview_length': 0
            }
            return self.vkclient.method('messages.getDialogs', values)

        def _get_chat_msg(message,offset,unread):
            values = {
            'offset': offset,
            'count': unread,
            'peer_id': 2000000000 + message['chat_id']
            }

            return self.vkclient.method('messages.getHistory', values)['items']

        def _get_user_msg(message,offset,unread):
            values = {
            'offset': offset,
            'count': unread,
            'user_id': message['user_id']
            }

            return self.vkclient.method('messages.getHistory', values)['items']

        def _get_items_pack(pack_length,count,function,function_arg=None):
            pack = []
            for i in range(0,count/pack_length):
                print(u'Pack #' + str(i) + u'. From ' + str(pack_length*i) +
                      u' to '+ str(pack_length*(i+1)-1) + u'.')
                if function_arg==None:
                    pack.extend(function(pack_length*i,pack_length)['items'])
                else:
                    pack.extend(function(function_arg,pack_length*i,pack_length))
            print(u'Last pack. From ' + str((count/pack_length)*pack_length) +
                  u' to '+ str((count/pack_length)*pack_length
                  + count%pack_length - 1) + u'.')
            if function_arg==None:
                pack.extend(function((count/pack_length)*pack_length,
                                    count%pack_length)['items'])
            else:
                pack.extend(function(function_arg,
                                    (count/pack_length)*pack_length,
                                    count%pack_length))


            # (count/pack_length)*pack_length можно заменить на
            # count-count%pack_length

            return pack


        ur_dialogs_count = _getDialogs_unread(0,0)['count']

        print (u'Unread dialogs count: '+str(ur_dialogs_count))

        pack = 100
        ur_dialogs = _get_items_pack(pack,ur_dialogs_count,_getDialogs_unread)

        for item in ur_dialogs:

            print(item['unread'])

            if 'chat_id' in item['message']:
                messages = _get_items_pack(pack,item['unread'],
                    _get_chat_msg,item['message'])
            else:
                messages = _get_items_pack(pack,item['unread'],
                    _get_user_msg,item['message'])

            for message in messages:
                self.message_handler(message)

            # Работает, но если только что написал и
            # бот почти сразу зашёл - реагирует дважды.

    def run(self):

        # попробовать вынести из BotCore
        #P001
        #print(u'Запуск сервиса доступа к vkapi.')

        #proc = Process(target=self.vkapi_service.run, args=())
        #proc.start()

        print(u'Обработка непрочитанного.')

        self.parce_unread()

        print(u'Приступаю к приему сообщений')

        # Нужно вынести в отдельную функцию. Возможно.
        # Добавить обработку своих сообщений. Что бы бот мог работать
        # на странице хозяина и реагировать на его запросы.
        while not self.core_exit_run:
            self.parce(0)
            if self.cmd_in_own_messages:
                self.parce(1)

            # обозначаем что аккаунт онлайн
            if self.be_online:
                values = {
                    'voip':0
                }
                self.vkclient.method('account.setOnline', values)

        print(u'Exit by `core_exit_run`.')
        self.core_kill_signal = False

        if self.set_offline:
            self.vkclient.method('account.setOffline')

        print(u'self.vkclient.send_exit()')
        if self.vkclient.send_exit():
            print (u'VkService exit ok.')
        else:
            print (u'VkService exit error.')

        #P001
        #print(u'wait proc exit')
        #proc.join()




    # скорее всего переделать. self.command реализовать во внешнем модуле.
    # parce(out) Читает сообщения из вк. Параметром out задаётся читать
    # входящие или исходящие сообщения.
    # Дальше передаёт сообщения обработчикам. Ничего не возвращает.
    def parce(self, out):
        # Скрытый баг! Связанный с работой метода vk api
        #
        # Если одновременно обратяться больше 20 пользователей, все кто
        # 21-й и выше - будут проигнорированны.
        #
        # https://vk.com/dev/messages.get :
        # идентификатор сообщения, полученного перед тем, которое нужно
        # вернуть последним (при условии,что после него было получено не
        # более count сообщений, иначе необходимо использовать с
        # параметром offset).
        # ** конец цитаты описания
        # Варианты режения
        # Смотреть разницу запомненого self.lastmessid и полученного после
        # выполнения запроса.
        # Если больше 20, ... :
        # 1. ..., то self.lastmessid сдвинуть только на 20.
        #    ? Не знаю как будет работать при переключении out=0 out=1 ?
        # 2. ..., закинуть полученные ответы в отдельный список, сделать
        # ещё запросы с изменением offset,
        #    добавляя сообщения в этот список.
        values = {
            'out': out,
            'offset': 0,
            'count': 20,
            'time_offset': 50,
            'filters': 0,
            'preview_length': 0,
            'last_message_id': self.lastmessid
        }

        # На будущее: обращаться не на прямую к vk_api, а через "очереди"
        # или что-то пободное
        response = self.vkclient.method('messages.get', values)

        if ('items' in response) and (len(response['items'])>0):        
            self.lastmessid = response['items'][0]['id']
            for message in response['items']:
                self.message_handler(message)
	

        time.sleep(0.34)



    # реализовать во внешнем модуле Oo или как?. (наверное там же
    # где и self.command)
    def commadn_execute(self, message, plugin, params):

        if plugin and plugin in self.commands:
            # Помечаем прочитанным перед выполнением команды.
            # Может быть вынести это ниже? Так он себя очень скрытно ведёт,
            # на несуществующие команды никак не реагирует.
            self.vkclient.markasread(message)

            # Приоритеты аргументов:
            # 0. message - всегла есть.
            # 1. params, with_args, args
            # 2. system

            # 0
            args = [message]

            # 1
            if any(s in self.commands[plugin].plugin_type for s in ['params',
                                                                    'with_args',
                                                                    'args']):
                args.append(params)
            # 2
            if 'settings' in self.commands[plugin].plugin_type:
                args.append(self.settings)
            # 3
            if 'system' in self.commands[plugin].plugin_type:
                args.append(self)

            self.commands[plugin].call(*args)
            return True
        else:
            return False

    # Получает сообщение. Передаёт каждому обработчику из списка, пока один
    # из них не обработает сообщение. Или пока не закончиться список.
    def message_handler(self, message):
        for handler in self.messagehandlers.values():
            result = handler.parce(message)

            # проверяем подходит ли сообщение под условия обработчика
            if result.has_key('plugin'):
                # Совпало - запускаем команду.
                self.commadn_execute(message, result['plugin'],
                    result['params'])
                return True

        # ни с чем не совпало
        return False

    def terminate(self, signal, frame):
        print(u'signal::'+str(signal))
        # На случай если программа зависла где-то там, где нет
        # слежения за self.core_exit_run
        if self.core_kill_signal:
            # При получении второго сигнала выходим
            print(u'Second signal. Exiting')
            sys.exit(0)
        else:
            # При получении первого сигнала - даём завершиться правильно.
            self.core_exit_run = True
            self.core_kill_signal = True
