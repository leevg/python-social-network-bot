#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard library imports
import argparse
from cookielib import CookieJar
from urllib import urlencode
from urllib2 import build_opener, HTTPCookieProcessor
import re
import json


# related third party imports
from lxml import html

class VkVideoGet:

   def __init__(self):
       self.HD_RES = {
             1: 360,
             2: 480,
             3: 720,
            }
       self.cj = CookieJar()
       self.opener = build_opener(HTTPCookieProcessor(self.cj))

   def __decode_html_entities(text):
       def fixup(m):
           text = m.group(0)
           if text[:2] == "&#":
              # character reference
              try:
                  if text[:3] == "&#x":
                     return unichr(int(text[3:-1], 16))
                  else:
                     return unichr(int(text[2:-1]))
              except ValueError:
                  pass
           else:
              try:
                  text = unichr(name2codepoint[text[1:-1]])
              except KeyError:
                  pass
           return text
       return re.sub("&#?\w+;", fixup, text)

   def login(self, email, pwd):
       resp = self.opener.open("http://vk.com")
       root = html.parse(resp)
       params = {node.attrib['name']:node.attrib.get('value', '')
                 for node in root.xpath('//form//input[@type!="submit"]')}
       params['email'] = email
       params['pass'] = pwd
       self.opener.open(root.xpath('//form')[0].attrib['action'], urlencode(params))

   def __old_methods(self, html_read):
       match = re.search('var\svars\s=\s(\{.+?\});', html_read)

       if match == None:
          return None

       data = json.loads(match.group(1).decode('windows-1251').encode('utf-8').replace('\\"', '"'))

       resolution = ''

       if isinstance(data['host'], basestring) and 'vkadre.ru' in data['host'] and not data['no_flv']:
           vurl = "http://{host}/assets/videos/{vtag}{vkid}.vk.flv".format(**data)
       else:
           if 'url720' in data :
             vurl = data['url720']
             resolution = '720p'
           elif 'url480' in data :
             vurl = data['url480']
             resolution = '480p'
           elif 'url360' in data :
             vurl = data['url360']
             resolution = '360p'
           elif 'url240' in data :
             vurl = data['url240']
             resolution = '240p'

       if len(resolution)>0:
             fname = decode_html_entities(data['md_title'].strip()) + '.' + \
                     resolution + '.'+ vurl.rpartition('.')[2]
       else:
             fname = decode_html_entities(data['md_title'].strip()) + '.' + \
                     vurl.rpartition('.')[2]

       fname = fname.split('?')[0]
       fname = fname.replace('/', '-')
       fname = fname.replace(':', '-')
       fname = fname.replace('?', '-')
       return {'url':vurl, 'fname':fname, 'resolution':resolution}

   def __new_methods(self, html_read):
       
       html5links = re.findall('<\s*source\ssrc\s*=\s*"(https*:\/\/.*?)"\s.*?>\s*<\/source>', html_read)
       mv_title = re.findall('<div\sclass\s*=\s*"mv_title"\sid\s*=\s*"mv_title"\s*>(.*?)<\/div>', html_read)[0].decode('windows-1251')
  
       links = {}

       for link in html5links:
           resol = re.findall('\.(\d*?)\.mp4',link)[0]
           links[resol] = link
           

       vurl = 'None'
       resolution = 'None'     

       if '720' in links :
             vurl = links['720']
             resolution = '720p'
       elif '480' in links :
             vurl = links['480']
             resolution = '480p'
       elif '360' in links :
             vurl = links['360']
             resolution = '360p'
       elif '240' in links :
             vurl = links['240']
             resolution = '240p'

       fname = mv_title + '.' + \
               resolution + '.mp4'
       
       fname = fname.replace('/', '-').replace(':', '-').replace('?', '-')

       return {'url':vurl, 'fname':fname, 'resolution':resolution}


   def get_url(self, url):
       _, _, vid = url.rpartition('/')
       if not vid.startswith('video'):
          return 'Error'

       params = {
           'act': 'show',
           'al': '1',
           'list': '',
           'module': 'video',
           'video': vid[5:].partition('?')[0]
       }

       resp = self.opener.open('http://vk.com/al_video.php', urlencode(params))
       html_read = resp.read()
       #resp = self.opener.open(url)
       #v_html_read = resp.read()

       #print html_read
       
       old = self.__old_methods(html_read)
       if old == None:
          print(u'New method')
          return self.__new_methods(html_read)
       else:
          print(u'Old method')
          return old

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--exec', dest='cmd')
    parser.add_argument('-l', '--login', dest='login')
    parser.add_argument('-p', '--password', dest='password')
    parser.add_argument('url')
    
    args = parser.parse_args()

    if args.login and args.password:

        vkvget = VkVideoGet()

        vkvget.login(args.login, args.password)
        result = vkvget.get_url(args.url)

        if args.cmd:
            run_cmd(args.cmd, result)
        
        else:
            print result['url']
            print result['fname']
            print result['resolution']
    else:
        print(u'No login and password.')
       


