# -*- coding: utf-8 -*-
# Класс с некоторыми доп. методами 

import sys
import pickle
import json
import zmq

from vkplus import VkPlus

# для обработки ошибок соединения
from requests import ConnectionError
import errno


class VkServiceServer:

    exit = False

    def __init__(self, vk_login, vk_pass, vk_app_id, port=43000):
        self.vkplus = VkPlus(vk_login, vk_pass, vk_app_id)
        # нужно выделить логин в отдельную функцию
        self.port = port

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind('tcp://127.0.0.1:'+str(self.port)) # вынести в параметры

        while not self.exit:
            try:
                command, key, data = pickle.loads(socket.recv())

            except Exception as e:
                print(u'Exeption at vkservice at load socket: ')
                print(e)
                socket.send(b'error')
                self.exit = True        # Для отладки, пусть крэшниться.

            # Взял решение от сюда http://stackoverflow.com/questions/2083987/how-to-retry-after-exception-in-python
            for attempt in range(10):
                if attempt > 0:
                    print (u'Attepts $'+str(attempt))

                try:
                    if command == 'respond':
                        result = self.vkplus.respond(key, data)                  
                        socket.send(pickle.dumps(result))
                    elif command == 'markasread':
                        result = self.vkplus.markasread(key) 
                        socket.send(pickle.dumps(result))
                    elif command == 'method':
                        #result = self.vkplus.api.method(key, data)
                        result = self.vkplus.method(key, data)
                        socket.send(pickle.dumps(result))
                    elif command == 'exit':
                        self.exit = True
                        socket.send(b'ok')
                    else:
                        print('unknow command: '+command)
                        socket.send(b'unknow command')

                #except Exception as e:
                except ConnectionError as e:
                    # тут нужно по ошибке ('Connection aborted.', error(104, 'Connection reset by peer'))
                    # сделать попытку повторного вызова метода.
                    # или даже в самом vk_api
                    if e.errno != errno.ECONNRESET:
                        print(u'Exeption at vkservice at attempt '+str(attempt)+u' :')
                        print(e)
                        break
                    print(u'104 Connection reset by peer. Attempt: '+str(attempt))                    
                else:
                    break
            else:
                print(u'Exeption at vkservice (last): ')
                print(e)
                socket.send(b'error')
                self.exit = True        # Для отладки, пусть крэшниться.


class VkServiceClient:

    def __init__(self,port=43000,timeout=10):       
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
	self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.connect('tcp://127.0.0.1:'+str(port))
	self.timeout = timeout

    def _recv(self):
        # Решение взял здесь https://github.com/zeromq/pyzmq/issues/132
        # Костыльное и не особо нравиться, но пусть пока что будет так.
        poller = zmq.Poller()
        poller.register(self.socket, zmq.POLLIN)
        if poller.poll(self.timeout*1000): 
           recive = self.socket.recv()
        else:
           raise IOError("Timeout processing auth request")
	

        if recive != b'error':
            return pickle.loads(recive)
        else:
            return {'error':'error'}

    def respond(self, key, data):
        self.socket.send(pickle.dumps(('respond', key, data)))
        return self._recv()

    def markasread(self, key):
        self.socket.send(pickle.dumps(('markasread', key, None)))
        return self._recv()

    def method(self, key, data=None):
        self.socket.send(pickle.dumps(('method', key, data)))
        return self._recv()

    def send_exit(self):
        self.socket.send(pickle.dumps(('exit', None, None)))
        return self.socket.recv() == b'ok'